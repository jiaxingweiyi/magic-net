﻿using Furion.DynamicApiController;
using Magic.Core;
using Magic.Core.Entity;
using Magic.Core.Service;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Magic.Web.Core;

/// <summary>
/// 系统参数配置服务
/// </summary>
[ApiDescriptionSettings(Name = "Config", Order = 100, Tag = "系统参数配置服务")]
public class SysConfigController : IDynamicApiController
{
    private readonly ISysConfigService _service;
    public SysConfigController(ISysConfigService service)
    {
        _service = service;
    }

    /// <summary>
    /// 分页获取系统参数配置
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    [HttpGet("/sysConfig/page")]
    public async Task<PageList<SysConfig>> PageList([FromQuery] QuerySysConfigPageInput input) { 
        return await _service.PageList(input);
    }

    /// <summary>
    /// 获取系统参数配置列表
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    [HttpGet("/sysConfig/list")]
    public async Task<List<SysConfig>> List([FromQuery] QuerySysConfigPageInput input) { 
        return await _service.List(input);
    }

    /// <summary>
    /// 增加系统参数配置
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    [HttpPost("/sysConfig/add")]
    public async Task Add(AddSysConfigInput input) { 
        await _service.Add(input);
    }

    /// <summary>
    /// 删除系统参数配置
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    [HttpPost("/sysConfig/delete")]
    public async Task Delete(Magic.Core.PrimaryKeyParam input) { 
        await _service.Delete(input);
    }

    /// <summary>
    /// 更新系统参数配置
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    [HttpPost("/sysConfig/edit")]
    public async Task Update(EditSysConfigInput input) { 
        await _service.Update(input);
    }

    /// <summary>
    /// 获取系统参数配置
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    [HttpGet("/sysConfig/detail")]
    public async Task<SysConfig> Get([FromQuery] Magic.Core.PrimaryKeyParam input) { 
        return await _service.Get(input);
    }
}

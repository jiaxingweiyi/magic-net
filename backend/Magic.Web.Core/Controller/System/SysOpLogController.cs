﻿using Furion.DynamicApiController;
using Magic.Core.Entity;
using Magic.Core;
using Magic.Core.Service;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace Magic.Web.Core;

/// <summary>
/// 操作日志服务
/// </summary>
[ApiDescriptionSettings(Name = "OpLog", Order = 100, Tag = "操作日志服务")]
public class SysOpLogController : IDynamicApiController
{
    private readonly ISysOpLogService _service;
    public SysOpLogController(ISysOpLogService service)
    {
        _service = service;
    }

    /// <summary>
    /// 分页查询操作日志
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    [HttpGet("/sysOpLog/page")]
    public async Task<PageList<SysLogOp>> PageList([FromQuery] QueryOpLogPageInput input)
    {
        return await _service.PageList(input);
    }

    /// <summary>
    /// 清空操作日志
    /// </summary>
    /// <returns></returns>
    [HttpPost("/sysOpLog/delete")]
    public async Task Clear()
    {
        await _service.Clear();
    }
}

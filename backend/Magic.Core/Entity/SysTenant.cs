﻿using SqlSugar;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
namespace Magic.Core.Entity;

/// <summary>
/// 租户表
/// </summary>
[SugarTable("sys_tenant")]
[Description("租户表")]
public class SysTenant : DEntityBase
{
    /// <summary>
    /// 公司名称
    /// </summary>
    [Required, MaxLength(30)]
    [SugarColumn(ColumnDescription = "公司名称", Length = 32)]
    public string Name { get; set; }

    /// <summary>
    /// 管理员名称
    /// </summary>
    [Required, MaxLength(32)]
    [SugarColumn(ColumnDescription = "管理员名称", Length = 32)]
    public string AdminName { get; set; }

    /// <summary>
    /// 主机
    /// </summary>
    [MaxLength(255)]
    [SugarColumn(ColumnDescription = "主机", IsNullable = true, Length = 255)]
    public string Host { get; set; }

    /// <summary>
    /// 电子邮箱
    /// </summary>
    [MaxLength(255)]
    [SugarColumn(ColumnDescription = "电子邮箱", IsNullable = true, Length = 255)]
    public string Email { get; set; }

    /// <summary>
    /// 电话
    /// </summary>
    [MaxLength(32)]
    [SugarColumn(ColumnDescription = "电话", IsNullable = true, Length = 32)]
    public string Phone { get; set; }

    /// <summary>
    /// 数据库连接
    /// </summary>
    [MaxLength(255)]
    [SugarColumn(ColumnDescription = "数据库连接", IsNullable = true, Length = 255)]
    public string Connection { get; set; }

    /// <summary>
    /// 架构
    /// </summary>
    [MaxLength(64)]
    [SugarColumn(ColumnDescription = "架构", IsNullable = true, Length = 64)]
    public string Schema { get; set; }

    /// <summary>
    /// 备注
    /// </summary>
    [MaxLength(255)]
    [SugarColumn(ColumnDescription = "备注", IsNullable = true, Length = 255)]
    public string Remark { get; set; }

    /// <summary>
    /// 租户类型
    /// </summary>
    [SugarColumn(ColumnDescription = "租户类型")]
    public TenantTypeEnum TenantType { get; set; }
}

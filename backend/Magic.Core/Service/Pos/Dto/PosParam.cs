﻿using System.ComponentModel.DataAnnotations;

namespace Magic.Core.Service;


#region 输入参数
public class QueryPosPageInput : PageParamBase
{
    /// <summary>
    /// 名称
    /// </summary>
    public string Name { get; set; }

    /// <summary>
    /// 编码
    /// </summary>
    public string Code { get; set; }
}


public class AddPosInput
{
    /// <summary>
    /// 名称
    /// </summary>
    [Required(ErrorMessage = "职位名称不能为空")]
    public string Name { get; set; }

    /// <summary>
    /// 编码
    /// </summary>
    [Required(ErrorMessage = "职位编码不能为空")]
    public string Code { get; set; }

    /// <summary>
    /// 排序
    /// </summary>
    public int Sort { get; set; }

    /// <summary>
    /// 备注
    /// </summary>
    public string Remark { get; set; }

}

public class EditPosInput : AddPosInput
{
    public long Id { get; set; }
}


#endregion
﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Magic.Core.Service;

/// <summary>
/// 异常日志参数
/// </summary>
public class QueryExLogPageInput : PageParamBase
{

    /// <summary>
    /// 名称
    /// </summary>
    public string Name { get; set; }

    /// <summary>
    /// 类名
    /// </summary>
    public string ClassName { get; set; }

    /// <summary>
    /// 方法名
    /// </summary>
    public string MethodName { get; set; }

    /// <summary>
    /// 异常信息
    /// </summary>
    public string ExceptionMsg { get; set; }

    /// <summary>
    /// 异常时间
    /// </summary>
    public DateTime ExceptionTime { get; set; }
}
